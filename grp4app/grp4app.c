/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <X11/Xutil.h>

#include <gmodule.h>
#include <gnome.h>
#include <gdk/gdkx.h>

#include <X11/cursorfont.h>

#include "libgnomekbd/gkbd-indicator-plugin.h"

#define PLUGIN_GCONF_DIR "/apps/" PACKAGE "/Grp4App"

#define CLASSES1_CONFIG_KEY  PLUGIN_GCONF_DIR "/classes1"
#define CLASSES2_CONFIG_KEY  PLUGIN_GCONF_DIR "/classes2"
#define GROUPS_CONFIG_KEY    PLUGIN_GCONF_DIR "/groups"

static GkbdIndicatorPluginContainer *container;

static guint listener;

static GtkBuilder *builder;

static GSList *classes1 = NULL;
static GSList *classes2 = NULL;
static GSList *groups = NULL;
static gchar **groupNames;

static GtkWindow *configParent;
static GtkWidget *configDialog, *windowList, *btnDel, *btnAdd,
    *defaultGroupsOMenu;
static GtkTreeModel *windowListModel;

static void
ResetStringList (GSList ** plist)
{
	while (*plist != NULL) {
		GSList *p = *plist;
		*plist = (*plist)->next;
		g_free (p->data);
		g_slist_free_1 (p);
	}
}

static void
ResetList (GSList ** plist)
{
	while (*plist != NULL) {
		GSList *p = *plist;
		*plist = (*plist)->next;
		g_slist_free_1 (p);
	}
}

static GSList *
LoadList (const gchar * key, GConfValueType type)
{
	GError *err = NULL;
	GSList *lst;

	lst =
	    gconf_client_get_list (container->conf_client, key, type,
				   &err);
	if (lst == NULL || err != NULL) {
		if (err != NULL) {
			g_warning ("Error reading configuration:%s\n",
				   err->message);
			g_error_free (err);
			err = NULL;
		}
	}
	return lst;
}

static void
LoadConfig (void)
{
	ResetStringList (&classes1);
	ResetStringList (&classes2);
	ResetList (&groups);

	classes1 = LoadList (CLASSES1_CONFIG_KEY, GCONF_VALUE_STRING);
	classes2 = LoadList (CLASSES2_CONFIG_KEY, GCONF_VALUE_STRING);
	groups = LoadList (GROUPS_CONFIG_KEY, GCONF_VALUE_INT);
}

static void
SaveConfig (void)
{
	GError *err = NULL;
	GConfChangeSet *cs = gconf_change_set_new ();
	gconf_change_set_set_list (cs,
				   CLASSES1_CONFIG_KEY, GCONF_VALUE_STRING,
				   classes1);
	gconf_change_set_set_list (cs, CLASSES2_CONFIG_KEY,
				   GCONF_VALUE_STRING, classes2);
	gconf_change_set_set_list (cs, GROUPS_CONFIG_KEY, GCONF_VALUE_INT,
				   groups);
	gconf_client_commit_change_set (container->conf_client, cs, TRUE,
					&err);
}

static void
ConfigChangedCallback (GConfClient * client,
		       guint cnxn_id, GConfEntry * entry)
{
	LoadConfig ();
}

static void
StartListening (void)
{
	GError *err = NULL;
	listener = gconf_client_notify_add (container->conf_client,
					    PLUGIN_GCONF_DIR,
					    (GConfClientNotifyFunc)
					    ConfigChangedCallback, NULL,
					    NULL, &err);
	if (0 == listener) {
		g_warning ("Error listening for configuration: [%s]\n",
			   err->message);
		g_error_free (err);
	}
}

static void
StopListening (void)
{
	gconf_client_notify_remove (container->conf_client, listener);
}

static gboolean
PluginInit (GkbdIndicatorPluginContainer * pc)
{
	GError *gerror = NULL;
	container = pc;

	gconf_client_add_dir (container->conf_client,
			      PLUGIN_GCONF_DIR,
			      GCONF_CLIENT_PRELOAD_NONE, &gerror);
	if (gerror != NULL) {
		g_error ("err: %s\n", gerror->message);
		g_error_free (gerror);
		gerror = NULL;
		return FALSE;
	}
	LoadConfig ();
	StartListening ();
	return TRUE;
}

static void
PluginTerm (void)
{
	StopListening ();

	ResetStringList (&classes1);
	ResetStringList (&classes2);
	ResetList (&groups);

	container = NULL;
}

static int
WindowCreated (const Window win, const Window parent)
{
	XClassHint *classHint = XAllocClassHint ();
	int group = -1;
	if (XGetClassHint (GDK_DISPLAY (), win, classHint) != 0) {
		char *wmClass1, *wmClass2;
		GSList *class1Iter = classes1, *class2Iter =
		    classes2, *groupIter = groups;
		wmClass1 = classHint->res_name;
		wmClass2 = classHint->res_class;
		printf ("Win: [%s][%s]\n", wmClass1, wmClass2);

		if (wmClass1 != NULL && wmClass2 != NULL) {
			int idx = 0;
			while (class1Iter != NULL && class2Iter != NULL
			       && groupIter != NULL) {
				const char *class1 =
				    (const char *) class1Iter->data;
				const char *class2 =
				    (const char *) class2Iter->data;
				if (class1 != NULL && class2 != NULL
				    && !strcmp (class1, wmClass1)
				    && !strcmp (class2, wmClass2)) {
/*                                      printf( "Found element %d\n", idx ); */
					group =
					    GPOINTER_TO_INT
					    (groupIter->data);
					break;
				}
				class1Iter = class1Iter->next;
				class2Iter = class2Iter->next;
				groupIter = groupIter->next;
				idx++;
			}
			XFree (wmClass1);
			XFree (wmClass2);
		}
	}
	XFree (classHint);

	printf ("asked for group %d\n", group);
	return group;
}

static int
GetWinListSelectedGroupNo (gboolean * anythingSelected)
{
	GtkTreeIter selectedRow;
	GtkTreeSelection *selection;
	int groupNo = -1;

	*anythingSelected = FALSE;
	selection =
	    gtk_tree_view_get_selection (GTK_TREE_VIEW (windowList));
	if (selection == NULL)
		return -1;
	if (!gtk_tree_selection_get_selected
	    (selection, &windowListModel, &selectedRow))
		return -1;
	gtk_tree_model_get (windowListModel, &selectedRow, 3, &groupNo,
			    -1);
	*anythingSelected = TRUE;
	return groupNo;
}

static void
WinClassSelected (GtkTreeSelection * selection, void *param)
{
	gboolean anythingSelected;
	int groupNo = GetWinListSelectedGroupNo (&anythingSelected);

	gtk_option_menu_set_history (GTK_OPTION_MENU (defaultGroupsOMenu),
				     groupNo + 1);
	gtk_widget_set_sensitive (btnDel, anythingSelected);
}

static void
DefaultGroupChanged (GtkWidget * defaultGroupsOMenuItem, void *param)
{
	GtkTreeSelection *selection;
	GtkTreePath *path;
	GtkTreeIter selectedRow;
	int groupNo, newGroupNo;
	gint *indices;
	gint selectedRowNum;
	GSList *selectedGroupNode;
	gboolean anythingSelected;

	groupNo = GetWinListSelectedGroupNo (&anythingSelected);
	if (!anythingSelected)
		return;

	newGroupNo =
	    gtk_option_menu_get_history (GTK_OPTION_MENU
					 (defaultGroupsOMenu)) - 1;

	if (newGroupNo == groupNo)
		return;

	selection =
	    gtk_tree_view_get_selection (GTK_TREE_VIEW (windowList));
	gtk_tree_selection_get_selected (selection, &windowListModel,
					 &selectedRow);
	path = gtk_tree_model_get_path (windowListModel, &selectedRow);
	indices = gtk_tree_path_get_indices (path);
	selectedRowNum = indices[0];
	gtk_tree_path_free (path);

	selectedGroupNode = g_slist_nth (groups, selectedRowNum);
	selectedGroupNode->data = GINT_TO_POINTER (newGroupNo);

	SaveConfig ();

	gtk_list_store_set (GTK_LIST_STORE (windowListModel), &selectedRow,
			    2,
			    newGroupNo >=
			    0 ? groupNames[newGroupNo] :
			    dgettext (PACKAGE, "not used"), 3, newGroupNo,
			    -1);
}

/*
 * Entirely taken from xlsfonts!
 */
static Window
SelectWindow ()
{
	int status;
	Cursor cursor;
	XEvent event;
	Window targetWin = None;
	int buttons = 0;
	Display *dpy = gdk_x11_get_default_xdisplay ();
	Window root = gdk_x11_get_default_root_xwindow ();

	/* Make the target cursor */
	cursor = XCreateFontCursor (dpy, XC_crosshair);

	/* Grab the pointer using target cursor, letting it room all over */
	status = XGrabPointer (dpy, root, False,
			       ButtonPressMask | ButtonReleaseMask,
			       GrabModeSync, GrabModeAsync, root, cursor,
			       CurrentTime);
	if (status != GrabSuccess)
		return (Window) 0;

	/* Let the user select a window... */
	while ((targetWin == None) || (buttons != 0)) {
		/* allow one more event */
		XAllowEvents (dpy, SyncPointer, CurrentTime);
		XWindowEvent (dpy, root,
			      ButtonPressMask | ButtonReleaseMask, &event);
		switch (event.type) {
		case ButtonPress:
			if (targetWin == None) {
				targetWin = event.xbutton.subwindow;	/* window selected */
				if (targetWin == None)
					targetWin = root;
			}
			buttons++;
			break;
		case ButtonRelease:
			if (buttons > 0)	/* there may have been some down before we started */
				buttons--;
			break;
		}
	}

	XUngrabPointer (dpy, CurrentTime);	/* Done with pointer */

	if (targetWin != root)
		targetWin = XmuClientWindow (dpy, targetWin);

	return targetWin;
}

static void
AddWindow (GtkWidget * btn, void *param)
{
	Window win = SelectWindow ();
	XClassHint *classHint;

	if (win == (Window) 0)
		return;

	classHint = XAllocClassHint ();
	if (XGetClassHint (GDK_DISPLAY (), win, classHint) != 0) {
		char *wmClass1, *wmClass2;
		wmClass1 = classHint->res_name;
		wmClass2 = classHint->res_class;

		if (wmClass1 != NULL && wmClass2 != NULL) {
			GtkTreeIter newRow;
			gboolean classAlreadyExists = FALSE;
			GSList *class1Iter = classes1, *class2Iter =
			    classes2;

			while (class1Iter != NULL && class2Iter != NULL) {
				const char *cn1 = class1Iter->data;
				const char *cn2 = class2Iter->data;
				if (!g_strcasecmp (cn1, wmClass1)
				    && !g_strcasecmp (cn2, wmClass2)) {
					classAlreadyExists = TRUE;
					break;
				}
				class1Iter = class1Iter->next;
				class2Iter = class2Iter->next;
			}

			if (!classAlreadyExists) {
				classes1 =
				    g_slist_append (classes1,
						    g_strdup (wmClass1));
				classes2 =
				    g_slist_append (classes2,
						    g_strdup (wmClass2));
				groups =
				    g_slist_append (groups,
						    GINT_TO_POINTER (-1));
				gtk_list_store_append (GTK_LIST_STORE
						       (windowListModel),
						       &newRow);
				gtk_list_store_set (GTK_LIST_STORE
						    (windowListModel),
						    &newRow, 0, wmClass1,
						    1, wmClass2, 2,
						    dgettext (PACKAGE,
							      "not used"),
						    3, -1, -1);
				SaveConfig ();
			} else {
				GtkWidget *msg =
				    gtk_message_dialog_new (GTK_WINDOW
							    (configDialog),
							    GTK_DIALOG_DESTROY_WITH_PARENT,
							    GTK_MESSAGE_WARNING,
							    GTK_BUTTONS_OK,
							    dgettext
							    (PACKAGE,
							     "Cannot add new window class. "
							     "The window class \"%s\", \"%s\" already exists in the list."),
							    wmClass1,
							    wmClass2);
				gtk_dialog_run (GTK_DIALOG (msg));
				gtk_widget_destroy (msg);
			}
			XFree (wmClass1);
			XFree (wmClass2);
		}
	}
	XFree (classHint);
}

static void
DelWindow (GtkWidget * btn, void *param)
{
	GtkTreeSelection *selection =
	    gtk_tree_view_get_selection (GTK_TREE_VIEW (windowList));
	GtkTreeIter selectedRow;
	GtkTreePath *path;
	gint *indices;
	gint selectedRowNum;

	gtk_tree_selection_get_selected (selection, &windowListModel,
					 &selectedRow);
	path = gtk_tree_model_get_path (windowListModel, &selectedRow);
	indices = gtk_tree_path_get_indices (path);
	selectedRowNum = indices[0];
	gtk_tree_path_free (path);

	classes1 =
	    g_slist_delete_link (classes1,
				 g_slist_nth (classes1, selectedRowNum));
	classes2 =
	    g_slist_delete_link (classes2,
				 g_slist_nth (classes2, selectedRowNum));
	groups =
	    g_slist_delete_link (groups,
				 g_slist_nth (groups, selectedRowNum));

	SaveConfig ();

	gtk_list_store_remove (GTK_LIST_STORE (windowListModel),
			       &selectedRow);
}

static void
ConfigurePlugin (GkbdIndicatorPluginContainer * pc, GtkWindow * parent)
{
	GtkWidget *defaultGroupsMenu, *menuItem;
	guint nGroups;
	gchar **groupName;
	int i;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *class1Col, *class2Col, *groupCol;
	GtkTreeSelection *selection;
	GSList *class1Iter, *class2Iter, *groupIter;
	GError *error = NULL;

	container = pc;
	configParent = parent;

	LoadConfig ();

	builder = gtk_builder_new ();
	gtk_builder_add_from_file (builder, UI_DIR "/grp4app.ui", &error);

	configDialog =
	    GTK_WIDGET (gtk_builder_get_object
			(builder, "grp4app_dialog"));
	gtk_window_set_modal (GTK_WINDOW (configDialog), TRUE);
	gtk_window_set_transient_for (GTK_WINDOW (configDialog), parent);

	defaultGroupsMenu = gtk_menu_new ();

	gtk_object_set_data (GTK_OBJECT (configDialog),
			     "defaultGroupsMenu", defaultGroupsMenu);

	defaultGroupsOMenu =
	    GTK_WIDGET (gtk_builder_get_object
			(builder, "defaultGroupsOMenu"));
	windowList =
	    GTK_WIDGET (gtk_builder_get_object (builder, "windowList"));
	btnAdd = GTK_WIDGET (gtk_builder_get_object (builder, "btnAdd"));
	btnDel = GTK_WIDGET (gtk_builder_get_object (builder, "btnDel"));

	menuItem =
	    gtk_menu_item_new_with_label (dgettext (PACKAGE, "not used"));
	gtk_object_set_data (GTK_OBJECT (menuItem), "group",
			     GINT_TO_POINTER (-1));

	g_signal_connect (GTK_OBJECT (menuItem), "activate",
			  G_CALLBACK (DefaultGroupChanged), NULL);

	gtk_menu_shell_append (GTK_MENU_SHELL (defaultGroupsMenu),
			       menuItem);
	gtk_widget_show (menuItem);
	nGroups = gkbd_indicator_plugin_get_num_groups (pc);
	groupNames = groupName =
	    gkbd_indicator_plugin_load_localized_group_names (pc);

	if (groupName != NULL) {
		while (*groupName != NULL) {
			menuItem =
			    gtk_menu_item_new_with_label (*groupName);
			gtk_object_set_data (GTK_OBJECT (menuItem),
					     "group", GINT_TO_POINTER (i));
			g_signal_connect (GTK_OBJECT (menuItem),
					  "activate",
					  G_CALLBACK (DefaultGroupChanged),
					  NULL);
			gtk_menu_shell_append (GTK_MENU_SHELL
					       (defaultGroupsMenu),
					       menuItem);
			gtk_widget_show (menuItem);
		}
		groupName++;
	}

	gtk_option_menu_set_menu (GTK_OPTION_MENU (defaultGroupsOMenu),
				  defaultGroupsMenu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (defaultGroupsOMenu),
				     0);

	renderer = gtk_cell_renderer_text_new ();
	class1Col =
	    gtk_tree_view_column_new_with_attributes (dgettext
						      (PACKAGE,
						       "Window class 1"),
						      renderer, "text", 0,
						      NULL);
	class2Col =
	    gtk_tree_view_column_new_with_attributes (dgettext
						      (PACKAGE,
						       "Window class 2"),
						      renderer, "text", 1,
						      NULL);
	groupCol =
	    gtk_tree_view_column_new_with_attributes (dgettext
						      (PACKAGE,
						       "Default group"),
						      renderer, "text", 2,
						      NULL);

	gtk_tree_view_append_column (GTK_TREE_VIEW (windowList),
				     class1Col);
	gtk_tree_view_append_column (GTK_TREE_VIEW (windowList),
				     class2Col);
	gtk_tree_view_append_column (GTK_TREE_VIEW (windowList), groupCol);

	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (windowList),
					   TRUE);
	selection =
	    gtk_tree_view_get_selection (GTK_TREE_VIEW (windowList));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	windowListModel =
	    GTK_TREE_MODEL (gtk_list_store_new
			    (4, G_TYPE_STRING, G_TYPE_STRING,
			     G_TYPE_STRING, G_TYPE_INT));

	gtk_tree_view_set_model (GTK_TREE_VIEW (windowList),
				 windowListModel);

	class1Iter = classes1;
	class2Iter = classes2;
	groupIter = groups;
	while (class1Iter != NULL && class2Iter != NULL
	       && groupIter != NULL) {
		GtkTreeIter iter;
		int group = GPOINTER_TO_INT (groupIter->data);
		if (group < 0 || group >= nGroups)
			group = -1;
		gtk_list_store_append (GTK_LIST_STORE (windowListModel),
				       &iter);
		gtk_list_store_set (GTK_LIST_STORE (windowListModel),
				    &iter, 0, class1Iter->data, 1,
				    class2Iter->data, 2,
				    group >=
				    0 ? groupNames[group] :
				    dgettext (PACKAGE, "not used"), 3,
				    group, -1);
		class1Iter = class1Iter->next;
		class2Iter = class2Iter->next;
		groupIter = groupIter->next;
	}

	g_signal_connect (G_OBJECT (selection), "changed",
			  G_CALLBACK (WinClassSelected), NULL);

	g_signal_connect (GTK_WIDGET
			  (gtk_builder_get_object (builder, "btnAdd")),
			  "clicked", G_CALLBACK (AddWindow), NULL);
	g_signal_connect (GTK_WIDGET
			  (gtk_builder_get_object (builder, "btnDel")),
			  "clicked", G_CALLBACK (DelWindow), NULL);

	gtk_tree_selection_unselect_all (selection);
	WinClassSelected (NULL, NULL);

	gtk_dialog_run (GTK_DIALOG (configDialog));
	gtk_widget_destroy (configDialog);
	configDialog = windowList = btnAdd = btnDel = NULL;
	container = NULL;
	configParent = NULL;
}

G_MODULE_EXPORT const GkbdIndicatorPlugin *
GetPlugin (void)
{
	static GkbdIndicatorPlugin gswitchitPlugin = {
		NULL
	};
	if (gswitchitPlugin.name == NULL) {
		bindtextdomain (PACKAGE, GNOMELOCALEDIR);
		bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

		gswitchitPlugin.name =
		    dgettext (PACKAGE, "Layout Per Application plugin");
		gswitchitPlugin.description =
		    dgettext (PACKAGE,
			      "Sets initial layout on per-application basis");
		gswitchitPlugin.init_callback = PluginInit;
		gswitchitPlugin.term_callback = PluginTerm;
		gswitchitPlugin.window_created_callback = WindowCreated;
		gswitchitPlugin.configure_properties_callback =
		    ConfigurePlugin;
	}
	return &gswitchitPlugin;
};
