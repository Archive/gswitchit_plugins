/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <gmodule.h>
#include <gnome.h>

#include "libgnomekbd/gkbd-indicator-plugin.h"

static void
GroupChanged (GtkWidget * notebook, int newGroup)
{
	char buf[128];
	g_snprintf (buf, sizeof (buf), "xkb_group_%d", (newGroup + 1));
	gnome_triggers_do ("", NULL, PACKAGE, buf, NULL);
}

static void
ConfigurePlugin (GkbdIndicatorPluginContainer * pc, GtkWindow * parent)
{
	gnome_execute_shell (NULL, "gnome-sound-properties");
}

G_MODULE_EXPORT const GkbdIndicatorPlugin *
GetPlugin (void)
{
	static GkbdIndicatorPlugin gswitchitPlugin = { NULL };
	if (gswitchitPlugin.name == NULL) {
		bindtextdomain (PACKAGE, GNOMELOCALEDIR);
		bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

		gswitchitPlugin.name = dgettext (PACKAGE, "Sound");
		gswitchitPlugin.description =
		    dgettext
		    (PACKAGE,
		     "Enables standard GNOME sound notifications for the group switching");
		/* functions */
		gswitchitPlugin.group_changed_callback = GroupChanged;
		gswitchitPlugin.configure_properties_callback =
		    ConfigurePlugin;
	}
	return &gswitchitPlugin;
};
