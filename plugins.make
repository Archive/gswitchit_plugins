#
# plugin_DATA should contain the list of plugins (plugin1.so plugin2.so)
#
                                                                                
plugindir = $(libdir)/gnomekbd

CFLAGS = $(plugin_cFLAGS)

LDFLAGS = $(plugin_ldFLAGS)

SUFFIXES = .c .so
.c.so:
	$(LIBTOOL) --mode=compile $(CC) $(CFLAGS) -c $< -o tmp$@.lo
	$(LIBTOOL) --mode=link    $(CC) $(CFLAGS) -o libtmp$@.la -rpath $(plugindir) tmp$@.lo $(LIBS) $(LDFLAGS) -module -avoid-version -export-dynamic
	@rm -f tmp$@.lo tmp$@.o libtmp$@.la
	@cp .libs/libtmp$@.so* $@
	@rm -f .libs/libtmp$@.*

clean-am distclean-am:
	rm -rf $(plugin_DATA) .libs
