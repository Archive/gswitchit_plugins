/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <gmodule.h>
#include <gnome.h>

#include "libgnomekbd/gkbd-indicator-plugin.h"

#define PLUGIN_GCONF_DIR "/apps/" PACKAGE "/CustomLabels"

#define LABEL_TEXT_CONFIG_KEY ( PLUGIN_GCONF_DIR "/LabelText" )

static GkbdIndicatorPluginContainer *container;

static GSList *layoutLabels = NULL;
static GtkTreeModel *layoutLabelsModel = NULL;

static guint listener;

static GtkBuilder *builder;

static void
LoadConfig (void)
{
	GError *gerror = NULL;
	if (layoutLabels != NULL) {
		g_slist_foreach (layoutLabels, (GFunc) g_free, NULL);
		g_slist_free (layoutLabels);
		layoutLabels = NULL;
	}

	layoutLabels =
	    gconf_client_get_list (container->conf_client,
				   LABEL_TEXT_CONFIG_KEY,
				   GCONF_VALUE_STRING, &gerror);
	if (gerror != NULL) {
		g_warning ("Error loading configuration: %s\n",
			   gerror->message);
		g_error_free (gerror);
		gerror = NULL;
	}
}

static void
SaveConfig (void)
{
	GConfChangeSet *cs;
	GError *gerror = NULL;

	cs = gconf_change_set_new ();

	gconf_change_set_set_list (cs, LABEL_TEXT_CONFIG_KEY,
				   GCONF_VALUE_STRING, layoutLabels);

	gconf_client_commit_change_set (container->conf_client, cs, TRUE,
					&gerror);
	if (gerror != NULL) {
		g_warning ("Error saving configuration: %s\n",
			   gerror->message);
		g_error_free (gerror);
		gerror = NULL;
	}
	gconf_change_set_unref (cs);
}

static void
ConfigChangedCallback (GConfClient * client,
		       guint cnxn_id, GConfEntry * entry)
{
	LoadConfig ();
	gkbd_indicator_plugin_container_reinit_ui (container);
}

static void
StartListening (void)
{
	GError *err = NULL;
	listener = gconf_client_notify_add (container->conf_client,
					    PLUGIN_GCONF_DIR,
					    (GConfClientNotifyFunc)
					    ConfigChangedCallback, NULL,
					    NULL, &err);
	if (0 == listener) {
		g_warning ("Error listening for configuration: [%s]\n",
			   err->message);
		g_error_free (err);
	}
}

static void
StopListening (void)
{
	gconf_client_notify_remove (container->conf_client, listener);
}

static gboolean
PluginInit (GkbdIndicatorPluginContainer * pc)
{
	GError *gerror = NULL;
	container = pc;

	gconf_client_add_dir (container->conf_client,
			      PLUGIN_GCONF_DIR,
			      GCONF_CLIENT_PRELOAD_NONE, &gerror);
	if (gerror != NULL) {
		g_error ("err: %s\n", gerror->message);
		g_error_free (gerror);
		gerror = NULL;
		return FALSE;
	}
	LoadConfig ();
	StartListening ();
	return TRUE;
}

static void
PluginTerm (void)
{
	StopListening ();
	container = NULL;
}

static void
AddLabelToList (gchar * labelText)
{
	GtkTreeIter iter;
	gtk_list_store_append (GTK_LIST_STORE (layoutLabelsModel), &iter);
	gtk_list_store_set (GTK_LIST_STORE (layoutLabelsModel), &iter, 0,
			    labelText, -1);
}

static void
UpdateList ()
{
	GtkWidget *list =
	    GTK_WIDGET (gtk_builder_get_object (builder, "lstLabels"));
	if (layoutLabelsModel == NULL) {
		layoutLabelsModel =
		    GTK_TREE_MODEL (gtk_list_store_new (1, G_TYPE_STRING));
		gtk_tree_view_set_model (GTK_TREE_VIEW (list),
					 GTK_TREE_MODEL
					 (layoutLabelsModel));
	} else {
		gtk_list_store_clear (GTK_LIST_STORE (layoutLabelsModel));
	}
	g_slist_foreach (layoutLabels, (GFunc) AddLabelToList, NULL);

}

static void
AddLabel (GtkButton * btn)
{
	GtkWidget *txtLabel =
	    GTK_WIDGET (gtk_builder_get_object (builder, "txtLabel"));
	layoutLabels =
	    g_slist_append (layoutLabels,
			    g_strdup (gtk_entry_get_text
				      (GTK_ENTRY (txtLabel))));
	gtk_entry_set_text (GTK_ENTRY (txtLabel), "");
	SaveConfig ();
	UpdateList ();
}

static void
DelLabel (GtkButton * btn)
{
	GtkWidget *list =
	    GTK_WIDGET (gtk_builder_get_object (builder, "lstLabels"));
	GtkTreeIter iter;

	GtkTreeSelection *selection =
	    gtk_tree_view_get_selection (GTK_TREE_VIEW (list));

	if (gtk_tree_selection_get_selected (selection, NULL, &iter)) {
		GtkTreePath *path =
		    gtk_tree_model_get_path (layoutLabelsModel, &iter);
		int idx = gtk_tree_path_get_indices (path)[0];
		gtk_tree_path_free (path);
		if (idx != -1) {
			GSList *deletable =
			    g_slist_nth (layoutLabels, idx);
			if (deletable != NULL) {
				g_free (deletable->data);
				layoutLabels =
				    g_slist_delete_link (layoutLabels,
							 deletable);
				SaveConfig ();
				UpdateList ();
			}
		}
	}
}

static void
ListSelectionChanged (GtkTreeSelection * selection)
{
	GtkWidget *btnDel =
	    GTK_WIDGET (gtk_builder_get_object (builder, "btnDelete"));

	GtkTreeIter selectedIter;
	gboolean isAnythingSelected = FALSE;


	if (gtk_tree_selection_get_selected
	    (selection, NULL, &selectedIter)) {
		isAnythingSelected = TRUE;
	}
	gtk_widget_set_sensitive (btnDel, isAnythingSelected);
}

static void
CfgDlgResponse (GtkWidget * dlg, gint response_id)
{
	if (response_id) {
		if (response_id == GTK_RESPONSE_CLOSE)
			gtk_widget_destroy (dlg);
		container = NULL;
	}
}

static void
ConfigurePlugin (GkbdIndicatorPluginContainer * pc, GtkWindow * parent)
{
	GtkWidget *dialog;
	GtkWidget *list;
	GtkTreeSelection *selection;
	GtkCellRenderer *renderer =
	    GTK_CELL_RENDERER (gtk_cell_renderer_text_new ());
	GtkTreeViewColumn *column =
	    gtk_tree_view_column_new_with_attributes (NULL, renderer,
						      "text", 0,
						      NULL);
	GError *error = NULL;

	container = pc;

	LoadConfig ();

	builder = gtk_builder_new ();
	gtk_builder_add_from_file (builder, UI_DIR "/custlbls.ui", &error);

	dialog =
	    GTK_WIDGET (gtk_builder_get_object
			(builder, "custlbls_dialog"));
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	g_signal_connect (gtk_builder_get_object (builder, "btnAdd"),
			  "clicked", G_CALLBACK (AddLabel), NULL);
	g_signal_connect (gtk_builder_get_object (builder, "btnDelete"),
			  "clicked", G_CALLBACK (DelLabel), NULL);
	g_signal_connect (G_OBJECT (dialog), "response",
			  G_CALLBACK (CfgDlgResponse), NULL);

	list = GTK_WIDGET (gtk_builder_get_object (builder, "lstLabels"));

	gtk_tree_view_append_column (GTK_TREE_VIEW (list), column);
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (list));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);

	g_signal_connect (G_OBJECT (selection), "changed",
			  G_CALLBACK (ListSelectionChanged), NULL);

	UpdateList ();

	gtk_widget_show (dialog);
}

static GtkWidget *
FindChildLabel (GtkWidget * widget)
{
	GtkWidget *child = widget;
	/* go down till first non-bin widget */
	while (GTK_IS_BIN (child)) {
		child = GTK_BIN (child)->child;
	}

	return GTK_IS_LABEL (child) ? child : NULL;
}

static GtkWidget *
ChangeLabel (GtkWidget * widget, const gint group, const char
	     *groupDescription, GkbdKeyboardConfig * config)
{
	GtkWidget *label = FindChildLabel (widget);

	if (label != NULL) {
		GSList *listItem = g_slist_nth (layoutLabels, group);
		gtk_label_set_text (GTK_LABEL (label),
				    listItem ==
				    NULL ? g_strdup_printf ("G%d",
							    group) :
				    listItem->data);
	}

	return widget;
}

G_MODULE_EXPORT const GkbdIndicatorPlugin *
GetPlugin (void)
{
	static GkbdIndicatorPlugin gswitchitPlugin = { NULL };
	if (gswitchitPlugin.name == NULL) {
		bindtextdomain (PACKAGE, GNOMELOCALEDIR);
		bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

		gswitchitPlugin.name = dgettext (PACKAGE, "Custom Labels");
		gswitchitPlugin.description =
		    dgettext (PACKAGE,
			      "Sets custom labels on the indicator");
		/* functions */
		gswitchitPlugin.init_callback = PluginInit;
		gswitchitPlugin.term_callback = PluginTerm;
		gswitchitPlugin.decorate_widget_callback = ChangeLabel;
		gswitchitPlugin.configure_properties_callback =
		    ConfigurePlugin;
	}
	return &gswitchitPlugin;
}
