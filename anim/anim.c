/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <math.h>
#include <gmodule.h>
#include <gnome.h>
#include <cairo.h>
#include <cairo-xlib.h>
#include <librsvg/rsvg.h>
#include <librsvg/rsvg-cairo.h>

#include <gdk/gdkx.h>

#include "libgnomekbd/gkbd-indicator-plugin.h"
#include "libgnomekbd/gkbd-indicator-config.h"

#define MASK_WIDTH (10.0)
#define MASK_HEIGHT (10.0)
#define N_SIN_POINTS (32)

/* For GConf: */
#define N_WAVES (4)
#define DELTA_TRANSPARENCY_PHASE (10)
#define DELTA_PHASE (60)
#define REDRAW_TIMEOUT (50)

#define ACTIVE_GROUP_PROPERTY ("animPlugin.activeGroup")
#define TRANSPARENCY_PHASE_PROPERTY ("animPlugin.transparencyPhase")
#define PHASE_PROPERTY ("animPlugin.phase")
#define FLAG_SURFACE_PROPERTY ("animPlugin.flagSurface")

#define STATIC_FLAG_PAGE (0)
#define WAVING_FLAG_PAGE (1)

extern gchar GKBD_INDICATOR_CONFIG_KEY_SHOW_FLAGS[];

static GkbdIndicatorPluginContainer *container = NULL;
static GkbdIndicatorConfig appletConfig;
static cairo_surface_t *maskSurface = NULL;

static GtkWidget *
GetDrawingArea (GtkWidget * notebook)
{
	return gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook),
					  WAVING_FLAG_PAGE);
}

static int
GetGroup (GtkWidget * mainNotebook)
{
	gpointer ptr = g_object_get_data (G_OBJECT (mainNotebook),
					  ACTIVE_GROUP_PROPERTY);
	return (ptr == NULL) ? -1 : GPOINTER_TO_INT (ptr) - 1;
}

static void
SetGroup (GtkWidget * mainNotebook, int group)
{
	/* printf ("SetGroup[%p] %d\n", mainNotebook, group); */
	g_object_set_data (G_OBJECT (mainNotebook), ACTIVE_GROUP_PROPERTY,
			   GINT_TO_POINTER (group + 1));
}

static void
SetFlagSurface (GtkWidget * notebook, cairo_surface_t * flagSurface)
{
	cairo_surface_reference (flagSurface);
	g_object_set_data_full (G_OBJECT (notebook), FLAG_SURFACE_PROPERTY,
				flagSurface,
				(GDestroyNotify) cairo_surface_destroy);
}

static cairo_surface_t *
GetFlagSurface (GtkWidget * notebook)
{
	return (cairo_surface_t *) g_object_get_data (G_OBJECT (notebook),
						      FLAG_SURFACE_PROPERTY);
}

static void
SetPhase (GtkWidget * drawingArea, int phase)
{
	/* printf ("SetPhase[%p]: %d\n", drawingArea, phase); */
	g_object_set_data (G_OBJECT (drawingArea), PHASE_PROPERTY,
			   GINT_TO_POINTER (phase));
}

static int
GetPhase (GtkWidget * drawingArea)
{
	return
	    GPOINTER_TO_INT (g_object_get_data
			     (G_OBJECT (drawingArea), PHASE_PROPERTY));
}

static void
SetTransparencyPhase (GtkWidget * drawingArea, int phase)
{
	/* printf ("SetTransparencyPhase[%p]: %d\n", drawingArea, phase); */
	g_object_set_data (G_OBJECT (drawingArea),
			   TRANSPARENCY_PHASE_PROPERTY,
			   GINT_TO_POINTER (phase));
}

static int
GetTransparencyPhase (GtkWidget * drawingArea)
{
	return
	    GPOINTER_TO_INT (g_object_get_data
			     (G_OBJECT (drawingArea),
			      TRANSPARENCY_PHASE_PROPERTY));
}

static void
IncPhase (GtkWidget * drawingArea)
{
	int phase = GetPhase (drawingArea) + DELTA_PHASE;
	while (phase >= 360)
		phase -= 360;
	SetPhase (drawingArea, phase);
}

static int
IncTransparencyPhase (GtkWidget * drawingArea, gboolean inc)
{
	int transparencyPhase = GetTransparencyPhase (drawingArea) +
	    (inc ? DELTA_TRANSPARENCY_PHASE : -DELTA_TRANSPARENCY_PHASE);
	SetTransparencyPhase (drawingArea, transparencyPhase);
	return transparencyPhase;
}

static void
FillSurface (GtkWidget * drawingArea,
	     cairo_surface_t * flagSurface,
	     gint xoffset,
	     gint yoffset,
	     gint width, gint height, int phase, int transparencyPhase)
{
	cairo_t *cr = gdk_cairo_create (drawingArea->window);
	int fw = cairo_image_surface_get_width (flagSurface);
	int fh = cairo_image_surface_get_height (flagSurface);
	/* printf ("Scaling flag %d X %d to frame %d X %d\n", fw, fh, 
	   width, height); */
	double wscale = ((double) width) / fw;
	double hscale = ((double) height) / fh;
	double scale = wscale < hscale ? wscale : hscale;
	/* printf ("Scale: %lf -> %d(%f) x %d(%f)\n", scale, 
	   (int)round(scale * fw), scale * fw, 
	   (int)round(scale * fh), scale * fh); */

	/* do 'wave' to tmp surface */
	cairo_surface_t *tmpSurface =
	    cairo_surface_create_similar (flagSurface,
					  CAIRO_CONTENT_COLOR_ALPHA, fw,
					  fh);
	cairo_t *tcr = cairo_create (tmpSurface);

	/* clear background */
	cairo_set_source_rgb (tcr, 1, 1, 1);
	cairo_set_source_rgb (cr, 1, 1, 1);

	cairo_set_source_surface (tcr, flagSurface, 0, 0);

	/* 'wave' transparency */
	cairo_scale (tcr, 2.0 * fw / MASK_WIDTH, fh / MASK_HEIGHT);
	cairo_mask_surface (tcr, maskSurface,
			    -phase / 360.0 * MASK_WIDTH / N_WAVES, 0);
	cairo_destroy (tcr);

	/* overall transparancy */
	cairo_translate (cr, xoffset + width * 0.5,
			 yoffset + height * 0.5);
	cairo_scale (cr, scale, scale);
	cairo_translate (cr, -0.5 * fw, -0.5 * fh);

	cairo_set_source_surface (cr, tmpSurface, 0, 0);

	cairo_pattern_t *tpat =
	    cairo_pattern_create_rgba (1, 1, 1, transparencyPhase / 100.0);
	cairo_mask (cr, tpat);
	cairo_destroy (cr);
	cairo_pattern_destroy (tpat);
	cairo_surface_destroy (tmpSurface);
}

static void
CreateMaskSurface (void)
{
	static unsigned char sinData[N_SIN_POINTS];	/*static!!! */
	cairo_surface_t *sinSurface = NULL;
	cairo_pattern_t *sinPat = NULL;
	cairo_matrix_t sinPatMatrix;
	unsigned char *pdata = sinData;
	int i, j = 0;

	for (i = N_SIN_POINTS; --i >= 0; pdata++, j++)
		*pdata =
		    (unsigned char) (0xFF *
				     (0.3 *
				      sin (j * 2 * M_PI / N_SIN_POINTS) +
				      0.70));

	sinSurface =
	    cairo_image_surface_create_for_data ((unsigned char *) sinData,
						 CAIRO_FORMAT_A8,
						 N_SIN_POINTS, 1,
						 N_SIN_POINTS << 2);
	sinPat = cairo_pattern_create_for_surface (sinSurface);

	cairo_matrix_init_scale (&sinPatMatrix,
				 1.0 * N_SIN_POINTS * N_WAVES / MASK_WIDTH,
				 1);
	cairo_pattern_set_matrix (sinPat, &sinPatMatrix);

	cairo_pattern_set_extend (sinPat, CAIRO_EXTEND_REPEAT);

	maskSurface =
	    cairo_image_surface_create (CAIRO_FORMAT_A8, MASK_WIDTH,
					MASK_HEIGHT);

	cairo_t *cr = cairo_create (maskSurface);

	cairo_rectangle (cr, 0, 0, MASK_WIDTH, MASK_HEIGHT);
	cairo_set_source (cr, sinPat);
	cairo_fill (cr);

	cairo_surface_destroy (sinSurface);
	cairo_destroy (cr);
	cairo_pattern_destroy (sinPat);
}

static gboolean
InitFunc (GkbdIndicatorPluginContainer * pc)
{
	/* printf("InitFunc\n"); */
	container = pc;
	GConfClient *confClient = gconf_client_get_default ();
	gkbd_indicator_config_init (&appletConfig,
				    confClient,
				    xkl_engine_get_instance (GDK_DISPLAY
							     ()));
	CreateMaskSurface ();
	appletConfig.show_flags = TRUE;
	g_object_unref (G_OBJECT (confClient));
	return TRUE;
}

static void
TermFunc ()
{
	/* printf("TermFunc\n"); */
	gkbd_indicator_config_term (&appletConfig);
	container = NULL;
}

static void
SetSignal (gpointer object, const gchar * signalName, GCallback func)
{
	gchar buf[20];
	g_snprintf (buf, sizeof (buf), "%s.id", signalName);

	gulong id = g_signal_connect (object, signalName, func, NULL);
	g_object_set_data (object, buf, GINT_TO_POINTER ((int) id));
}

#if 0
static void
RemoveSignal (gpointer object, const gchar * signalName)
{
	gchar buf[20];
	gulong id;

	g_snprintf (buf, sizeof (buf), "%s.id", signalName);

	id = (gulong) GPOINTER_TO_INT (g_object_get_data (object, buf));
	if (id == 0L)
		return;

	g_signal_handler_disconnect (object, id);
}
#endif

static gboolean
ShowRealWidget (GtkWidget * notebook)
{
	gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook),
				       STATIC_FLAG_PAGE);

	return FALSE;
}

static gboolean
GrowPhase (GtkWidget * notebook)
{
	/* printf ("GrowPhase\n"); */
	GtkWidget *drawingArea = GetDrawingArea (notebook);
	IncPhase (drawingArea);
	int transparencyPhase = IncTransparencyPhase (drawingArea, TRUE);
	gtk_widget_queue_draw (drawingArea);

	if (transparencyPhase <= 100)
		return TRUE;

	ShowRealWidget (notebook);
	return FALSE;
}

static gboolean
ShrinkPhase (GtkWidget * notebook)
{
	/* printf ("ShrinkPhase\n"); */
	GtkWidget *drawingArea = GetDrawingArea (notebook);
	IncPhase (drawingArea);
	int transparencyPhase = IncTransparencyPhase (drawingArea, FALSE);
	gtk_widget_queue_draw (drawingArea);

	if (transparencyPhase > 20)
		return TRUE;

	cairo_surface_t *activeFlagSurface = GetFlagSurface (notebook);
	if (activeFlagSurface == NULL)
		return TRUE;

	SetFlagSurface (drawingArea, activeFlagSurface);
	g_timeout_add (REDRAW_TIMEOUT, (GSourceFunc) GrowPhase, notebook);
	return FALSE;
}

static cairo_surface_t *
ReadFlagSurface (char *filename)
{
	cairo_t *cr;
	cairo_surface_t *flagSurface;
	GError *err = NULL;
	RsvgHandle *rsvg;
	RsvgDimensionData dims;

	rsvg = rsvg_handle_new_from_file (filename, &err);
	if (err != NULL) {
		g_warning ("Error reading flag svg: %s\n", err->message);
		g_error_free (err);
		return NULL;
	}

	rsvg_handle_get_dimensions (rsvg, &dims);

	flagSurface =
	    cairo_image_surface_create (CAIRO_FORMAT_ARGB32, dims.width,
					dims.height);
	cr = cairo_create (flagSurface);
	rsvg_handle_render_cairo (rsvg, cr);
	g_object_unref (rsvg);

	cairo_destroy (cr);
	return flagSurface;
}

static void
GroupChanged (GtkWidget * mainNotebook, gint activeGroup)
{
	int activePage = activeGroup + 1;
	/* printf ("activeGroup: %d\n", activeGroup); */
/*  first page is not used by the indicator widget */
	if (activePage == 0)
		return;

	GtkWidget *notebook =
	    gtk_notebook_get_nth_page (GTK_NOTEBOOK (mainNotebook),
				       activePage);

	/* printf ("notebook: %p\n", notebook); */

	/* no widget - or not handled by this plugin */
	if (notebook == NULL || !GTK_IS_NOTEBOOK (notebook))
		return;

	int prevGroup = GetGroup (mainNotebook);
	/*  sometimes we are notified about changes from N to N -just ignore them! */
	if (prevGroup == activeGroup)
		return;

	SetGroup (mainNotebook, activeGroup);

	int prevPage = prevGroup + 1;
	GtkWidget *prevNotebook =
	    gtk_notebook_get_nth_page (GTK_NOTEBOOK (mainNotebook),
				       prevPage);
	/* printf ("prevGroup: %d\n", prevGroup); */
	/* printf ("prevPage: %d\n", prevPage); */
	/* printf ("prevNotebook: %p\n", prevNotebook); */
	if (prevNotebook == NULL)
		return;

	cairo_surface_t *prevFlagSurface = GetFlagSurface (prevNotebook);
	/* printf ("prevFlagSurface: %p\n", prevFlagSurface); */
	if (prevFlagSurface == NULL)
		return;

	gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook),
				       WAVING_FLAG_PAGE);

	GtkWidget *drawingArea = GetDrawingArea (notebook);
	/* printf ("drawing area sizes: %d X %d\n", drawingArea->allocation.width, drawingArea->allocation.height); */

	SetFlagSurface (drawingArea, prevFlagSurface);

	SetPhase (drawingArea, 0);
	SetTransparencyPhase (drawingArea, 100);

	g_timeout_add (REDRAW_TIMEOUT, (GSourceFunc) ShrinkPhase,
		       notebook);
}

static gboolean
DrawingAreaExpose (GtkWidget * drawingArea)
{
	FillSurface (drawingArea,
		     GetFlagSurface (drawingArea),
		     0, 1,
		     drawingArea->allocation.width,
		     drawingArea->allocation.height - 2,
		     GetPhase (drawingArea),
		     GetTransparencyPhase (drawingArea));

	return TRUE;
}

static GtkWidget *
DecorateWidget (GtkWidget * widget,
		const gint group,
		const char *groupDescription, GkbdKeyboardConfig * config)
{
	/* printf ("DecorateWidget for %p\n", widget); */
	GtkWidget *notebook, *drawingArea;

	if (!gconf_client_get_bool
	    (container->conf_client, GKBD_INDICATOR_CONFIG_KEY_SHOW_FLAGS,
	     NULL))
		/* Ignore this plugin */
		return NULL;

	notebook = gtk_notebook_new ();
	gtk_notebook_set_show_border (GTK_NOTEBOOK (notebook), FALSE);
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), FALSE);

	/* Page itself: 0 */
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), widget, NULL);

	/* drawing area: 1 */
	drawingArea = gtk_drawing_area_new ();
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), drawingArea,
				  NULL);

	SetSignal (G_OBJECT (drawingArea),
		   "expose-event", G_CALLBACK (DrawingAreaExpose));

	gtk_notebook_set_current_page (GTK_NOTEBOOK (notebook),
				       STATIC_FLAG_PAGE);

	/* printf ("Created notebook: %p for group %d/%s\n", notebook, group, groupDescription); */

	gchar *imgFile =
	    gkbd_indicator_config_get_images_file (&appletConfig, config,
						   group);
	/* printf ("The file for the group %d(%s) is [%s]\n", group, groupDescription, imgFile); */
	if (imgFile != NULL) {
		cairo_surface_t *flagSurface = ReadFlagSurface (imgFile);
		/* printf ("Cairo surface for %p: %p\n", notebook, flagSurface); */
		SetFlagSurface (notebook, flagSurface);
		/* now flagSurface is owned by the notebook */
		cairo_surface_destroy (flagSurface);
	}
	/*printf ("DecorateWidget for %p returns %p\n", widget, notebook); */
	return notebook;
}

G_MODULE_EXPORT const GkbdIndicatorPlugin *
GetPlugin (void)
{
/* printf ("Get ANIM Plugin\n"); */
	static GkbdIndicatorPlugin gswitchitPlugin = { NULL };
	if (gswitchitPlugin.name == NULL) {
		bindtextdomain (PACKAGE, GNOMELOCALEDIR);
		bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

		gswitchitPlugin.name = dgettext (PACKAGE, "Animation");
		gswitchitPlugin.description =
		    dgettext (PACKAGE, "Animation plugin");
		gswitchitPlugin.init_callback = InitFunc;
		gswitchitPlugin.term_callback = TermFunc;
		gswitchitPlugin.decorate_widget_callback = DecorateWidget;
		gswitchitPlugin.group_changed_callback = GroupChanged;
	}
	return &gswitchitPlugin;
};
