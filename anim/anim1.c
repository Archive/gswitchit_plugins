#include <math.h>
#include <cairo.h>
#include <cairo-xlib.h>
#include <svg-cairo.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#define MASK_WIDTH 10.0
#define MASK_HEIGHT 10.0
#define N_WAVES 4
#define N_SIN_POINTS 32
#define REDRAW_TIMEOUT 100

#define N_FLAGS 2

static cairo_surface_t *flag_surfaces[N_FLAGS];
static cairo_surface_t *mask_surface = NULL;


static double phase = 0.0;

static double transparency_phase = 0.0;
static double dtphase = 0.10;

static int currentFlagIdx = 0;

static void
inc_phase (void)
{
	phase += 60.0;
	while (phase >= 360.0)
		phase -= 360.0;

}

static gboolean
grow_phase (GtkWidget * drawing)
{
	inc_phase ();
	transparency_phase += dtphase;
	gtk_widget_queue_draw (drawing);

	return transparency_phase <= 1.0;
}

static gboolean
shrink_phase (GtkWidget * drawing)
{
	inc_phase ();
	transparency_phase -= dtphase;
	gtk_widget_queue_draw (drawing);

	if (transparency_phase > 0.2)
		return TRUE;

	currentFlagIdx = 1 - currentFlagIdx;
	g_timeout_add (REDRAW_TIMEOUT, (GSourceFunc)grow_phase, drawing);
	return FALSE;
}
	
static void 
fill_surface (GtkWidget * win, int idx, gint width, gint height)
{
	cairo_surface_t *flag_surface = flag_surfaces[idx];
	cairo_t *cr = gdk_cairo_create (win->window);
	int fw = cairo_image_surface_get_width (flag_surface);
	int fh = cairo_image_surface_get_height (flag_surface);
	double wscale = ((double)width)/fw;
	double hscale = ((double)height)/fh;
	double scale = wscale < hscale ? wscale : hscale;

	/* do 'wave' to tmp surface */
	cairo_surface_t *tmp_surface = cairo_surface_create_similar (flag_surface, CAIRO_CONTENT_COLOR_ALPHA, fw, fh);
	cairo_t * tcr = cairo_create (tmp_surface);
	
	/* clear background */
	cairo_set_source_rgb (tcr, 1, 1, 1);
	cairo_set_source_rgb (cr, 1, 1, 1);

	cairo_set_source_surface (tcr, flag_surface, 0, 0);
	
	/* 'wave' transparency */
	cairo_scale (tcr, 2.0 * fw/MASK_WIDTH, fh/MASK_HEIGHT);
	cairo_mask_surface (tcr, mask_surface, - phase/360 * MASK_WIDTH/N_WAVES, 0);
	cairo_destroy (tcr);

	/* overall transparancy */
	cairo_translate (cr, width * 0.5, height * 0.5);
	cairo_scale (cr, scale, scale);
	cairo_translate (cr, -0.5 * fw, -0.5 * fh);

	cairo_set_source_surface (cr, tmp_surface, 0, 0);

	cairo_pattern_t * tpat = cairo_pattern_create_rgba (1, 1, 1, transparency_phase);
	cairo_mask (cr, tpat);
	cairo_destroy (cr);
	cairo_pattern_destroy (tpat);
        cairo_surface_destroy (tmp_surface);
}

static gboolean 
paint_the_stuff (GtkWidget * drawing,
		 GdkEventExpose * event, void * userdata)
{
	fill_surface (drawing, currentFlagIdx, drawing->allocation.width, drawing->allocation.height);

	return TRUE;
}

static void
create_mask_surface ()
{
	static unsigned char sin_data[N_SIN_POINTS]; /*static!!!*/
	cairo_surface_t *sin_surface = NULL;
	cairo_pattern_t *sin_pat = NULL;
	cairo_matrix_t sin_pat_matrix;
	unsigned char *pdata = sin_data;
	int i, j = 0;

	for (i = N_SIN_POINTS; --i>=0; pdata++, j++)
		*pdata = (unsigned char)(0xFF * (0.3 * sin (j * 2 * M_PI / N_SIN_POINTS) + 0.70 ));

	sin_surface = cairo_image_surface_create_for_data ((unsigned char*)sin_data, 
							   CAIRO_FORMAT_A8,
							   N_SIN_POINTS, 1, 
							   N_SIN_POINTS << 2);
	sin_pat = cairo_pattern_create_for_surface (sin_surface);

	cairo_matrix_init_scale (&sin_pat_matrix, 
				 1.0 * N_SIN_POINTS * N_WAVES / MASK_WIDTH,
				 1);
	cairo_pattern_set_matrix (sin_pat, &sin_pat_matrix);

	cairo_pattern_set_extend (sin_pat, CAIRO_EXTEND_REPEAT);

	mask_surface = cairo_image_surface_create (CAIRO_FORMAT_A8, MASK_WIDTH, MASK_HEIGHT);

	cairo_t *cr = cairo_create (mask_surface);

	cairo_rectangle (cr, 0, 0, MASK_WIDTH, MASK_HEIGHT);
	cairo_set_source (cr, sin_pat);
	cairo_fill (cr);

	cairo_surface_destroy (sin_surface);
	cairo_destroy (cr);
	cairo_pattern_destroy (sin_pat);
}

static void
read_flag_surface (char* filename, int idx)
{
	cairo_surface_t **flag_surface = flag_surfaces + idx;
	unsigned w, h;
	svg_cairo_t *svgc;
	cairo_t *cr;
	FILE * fh = fopen (filename, "r");

	svg_cairo_create (&svgc);
printf("svgc: %p, file(%s): %p\n",svgc, filename, fh);
	svg_cairo_parse_file (svgc, fh);
	fclose (fh);

	svg_cairo_get_size (svgc, &w, &h);
printf("svgcairo size: %d x %d\n", w, h);
	*flag_surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, w, h);
	cr = cairo_create (*flag_surface);
	svg_cairo_render (svgc, cr);
	svg_cairo_destroy (svgc);
	cairo_destroy (cr);
}

static void
switch_flag (GtkWidget * button, GtkWidget * drawing)
{
	g_timeout_add (REDRAW_TIMEOUT, (GSourceFunc)shrink_phase, drawing);
}

int
main (int argc, char *argv[]) {
	int i;

	gtk_init(&argc,&argv);

	read_flag_surface ("ru.svg", 0);
	read_flag_surface ("us.svg", 1);
	create_mask_surface ();

	GtkWidget * win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	printf("new win %p\n", win);
	gtk_window_set_title (GTK_WINDOW (win), "Hello Cairo");
	GtkWidget * drawing = gtk_drawing_area_new ();
	GtkWidget * container = gtk_vbox_new (TRUE, 6);
	GtkWidget * button = gtk_button_new_with_label ("Change It!");
	printf("new drawing %p\n", drawing);
	gtk_container_add (GTK_CONTAINER(container), drawing);
	gtk_container_add (GTK_CONTAINER(container), button);
	gtk_container_add (GTK_CONTAINER(win), container);

	g_signal_connect (win, "destroy", gtk_main_quit, NULL);
	g_signal_connect (button, "clicked", G_CALLBACK (switch_flag), drawing);
	g_signal_connect (drawing, "expose-event", G_CALLBACK (paint_the_stuff), NULL);

	currentFlagIdx = 0;
	g_timeout_add (REDRAW_TIMEOUT, (GSourceFunc)grow_phase, drawing);
	gtk_widget_show_all (win);

	gtk_main();

	for (i=N_FLAGS; --i>=0;)
	{
	        cairo_surface_destroy (flag_surfaces[i]);
	}
        cairo_surface_destroy (mask_surface);

        return 0;
}
