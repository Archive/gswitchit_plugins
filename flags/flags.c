/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <unistd.h>

#include <gmodule.h>
#include <gnome.h>

#include <libsoup/soup.h>

#include "libgnomekbd/gkbd-indicator-plugin.h"

extern gchar GKBD_INDICATOR_CONFIG_KEY_SHOW_FLAGS[];

#ifdef LOCALURL
#define FLAGS_URL "http://localhost/sodipodi-flags-1.6.tar.bz2"
#else
#define FLAGS_URL "http://internap.dl.sourceforge.net/sourceforge/sodipodi/sodipodi-flags-1.6.tar.bz2"
#endif

#define SCRIPTER_EXEC "perl"

static GkbdIndicatorPluginContainer *container;

static gboolean showFlags = FALSE;

static guint listener;

static GtkBuilder *builder = NULL;

static SoupSession *soupSession = NULL;
static SoupMessage *soupMessage = NULL;

static void
LoadConfig (void)
{
	GError *gerror = NULL;
	showFlags =
	    gconf_client_get_bool (container->conf_client,
				   GKBD_INDICATOR_CONFIG_KEY_SHOW_FLAGS,
				   &gerror);
	if (gerror != NULL) {
		if (gerror != NULL) {
			g_warning ("Error reading configuration:%s\n",
				   gerror->message);
			g_error_free (gerror);
			gerror = NULL;
		}
	}
}

static void
SaveConfig (void)
{
	GConfChangeSet *cs;
	GError *gerror = NULL;

	cs = gconf_change_set_new ();

	gconf_change_set_set_bool (cs,
				   GKBD_INDICATOR_CONFIG_KEY_SHOW_FLAGS,
				   showFlags);

	gconf_client_commit_change_set (container->conf_client, cs, TRUE,
					&gerror);
	if (gerror != NULL) {
		g_warning ("Error saving configuration: %s\n",
			   gerror->message);
		g_error_free (gerror);
		gerror = NULL;
	}
	gconf_change_set_unref (cs);
}

static void
ConfigChangedCallback (GConfClient * client,
		       guint cnxn_id, GConfEntry * entry, int isCfg)
{
	LoadConfig ();

	if (isCfg) {
		if (builder != NULL) {
			GtkWidget *chkBtn =
			    GTK_WIDGET (gtk_builder_get_object
					(builder, "chkEnableFlags"));
			if (chkBtn != NULL) {
				gtk_toggle_button_set_active
				    (GTK_TOGGLE_BUTTON (chkBtn),
				     showFlags);
			}
		}
	} else
		gkbd_indicator_plugin_container_reinit_ui (container);
}

static void
StartGConfListening (gboolean isCfgDialog)
{
	GError *err = NULL;
	listener = gconf_client_notify_add (container->conf_client,
					    GKBD_INDICATOR_CONFIG_KEY_SHOW_FLAGS,
					    (GConfClientNotifyFunc)
					    ConfigChangedCallback,
					    GINT_TO_POINTER (isCfgDialog ?
							     1 : 0), NULL,
					    &err);
	if (0 == listener) {
		g_warning ("Error listening for configuration: [%s]\n",
			   err->message);
		g_error_free (err);
	}
}

static void
StopListening (void)
{
	gconf_client_notify_remove (container->conf_client, listener);
}

static gboolean
PluginInit (GkbdIndicatorPluginContainer * pc)
{
	GError *gerror = NULL;
	container = pc;

	gconf_client_add_dir (container->conf_client,
			      GKBD_INDICATOR_CONFIG_KEY_SHOW_FLAGS,
			      GCONF_CLIENT_PRELOAD_NONE, &gerror);
	if (gerror != NULL) {
		g_error ("err: %s\n", gerror->message);
		g_error_free (gerror);
		gerror = NULL;
		return FALSE;
	}
	LoadConfig ();
	StartGConfListening (FALSE);
	return TRUE;
}

static void
PluginTerm (void)
{
	StopListening ();
	container = NULL;
}

static void
ChangeShowFlag (GtkCheckButton * chkBtn)
{
	showFlags =
	    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (chkBtn));
	SaveConfig ();
}

static void
SetDownloadStatus (const char *status)
{
	gtk_label_set_text (GTK_LABEL (gtk_builder_get_object
				       (builder, "lblDownloadStatus")),
			    status);
	gtk_main_iteration ();
}

static void
EnableDisableDownloadButtons (gboolean inProcess)
{
	gtk_widget_set_sensitive (GTK_WIDGET (gtk_builder_get_object
					      (builder,
					       "btnStartDownload")),
				  !inProcess);
	gtk_widget_set_sensitive (GTK_WIDGET
				  (gtk_builder_get_object
				   (builder, "btnCancelDownload")),
				  inProcess);
}

static void
GotFile (SoupSession * session, SoupMessage * msg)
{
	gchar *errMsg = NULL;

	int currentSize =
	    GPOINTER_TO_INT (g_object_get_data
			     (G_OBJECT (msg), "currentSize"));
	int totalSize =
	    GPOINTER_TO_INT (g_object_get_data
			     (G_OBJECT (msg), "totalSize"));

	int responseBodyLength = msg->response_body->length;
	printf
	    ("Got the file %s, resp body length %d/current size %d/total size %d\n",
	     soup_message_get_uri (msg)->path, responseBodyLength,
	     currentSize, totalSize);
	printf ("status: %d [%s]\n", msg->status_code,
		soup_status_get_phrase (msg->status_code));

	if (SOUP_STATUS_IS_SUCCESSFUL (msg->status_code)) {
		if (currentSize == responseBodyLength &&
		    (totalSize == 0 || totalSize == currentSize)) {
			/* Only complete files are processed! */
			gchar *tarname =
			    g_build_filename (g_get_tmp_dir (),
					      "sodipodi-flagsXXXXXX",
					      NULL);
			int fd = mkstemp (tarname);
			gchar *perlPath;
			write (fd, msg->response_body->data,
			       responseBodyLength);
			close (fd);
			printf ("Copied to [%s]\n", tarname);
			perlPath = g_find_program_in_path (SCRIPTER_EXEC);
			if (perlPath) {
				gchar *cmdLine =
				    g_strdup_printf
				    ("%s %s/flags.pl %s %s",
				     perlPath, UI_DIR,
				     tarname, g_get_tmp_dir ());
				GError *err = NULL;
				gint exitStatus;
				printf ("Command line: [%s]\n", cmdLine);
				SetDownloadStatus (dgettext
						   (PACKAGE,
						    "Unpacking ..."));
				if (g_spawn_command_line_sync
				    (cmdLine, NULL, NULL, &exitStatus,
				     &err)) {
					printf ("Exit code: [%d]\n",
						exitStatus);
					switch (exitStatus) {
					case 0:
						{
							/* ok */
							int totalSize =
							    GPOINTER_TO_INT
							    (g_object_get_data
							     (G_OBJECT
							      (msg),
							      "totalSize"));
							int newVal =
							    totalSize ==
							    0 ? 2 : ((int)
								     totalSize
								     /
								     0.75);
							gtk_range_set_value
							    (GTK_RANGE
							     (gtk_builder_get_object
							      (builder,
							       "downloadProgress")),
							     newVal);
							break;
						}
					default:
						errMsg =
						    g_strdup_printf
						    (dgettext
						     (PACKAGE,
						      "Error on the archive unpacking: %d"),
						     exitStatus);
						break;
					}
				} else {
					errMsg =
					    g_strdup_printf
					    (dgettext
					     (PACKAGE,
					      "Script execution error %d: %s"),
					     err->code, err->message);
				}

				g_free (cmdLine);
				g_free (perlPath);
			} else {
				errMsg =
				    g_strdup_printf
				    (dgettext
				     (PACKAGE,
				      "Interpreter %s not found"),
				     SCRIPTER_EXEC);
			}
			unlink (tarname);
			g_free (tarname);
		}
	} else
		errMsg =
		    g_strdup_printf
		    (dgettext
		     (PACKAGE,
		      "Error %d while downloading the archive: %s"),
		     msg->status_code, msg->reason_phrase);

	EnableDisableDownloadButtons (FALSE);
	if (errMsg) {
		SetDownloadStatus (errMsg);
		g_free (errMsg);
	} else {
		SetDownloadStatus (dgettext (PACKAGE, "Done"));
	}
}

static void
GotHeaders (SoupMessage * msg)
{
	gint cl = (gint)
	    soup_message_headers_get_content_length
	    (msg->response_headers);
	printf ("content length: %d\n", cl);
	if (cl != 0) {
		g_object_set_data (G_OBJECT (msg), "totalSize",
				   GINT_TO_POINTER (cl));
		gtk_range_set_range (GTK_RANGE
				     (gtk_builder_get_object
				      (builder, "downloadProgress")), 0,
				     (int) (cl * 3 / 4));
	} else
		gtk_range_set_range (GTK_RANGE (gtk_builder_get_object
						(builder,
						 "downloadProgress")), 0,
				     2);
	g_object_set_data (G_OBJECT (msg), "currentSize",
			   GINT_TO_POINTER (0));
	gtk_range_set_value (GTK_RANGE (gtk_builder_get_object
					(builder, "downloadProgress")), 0);
}

static void
GotMsgChunk (SoupMessage * msg)
{
	int cs = msg->response_body->length;
	g_object_set_data (G_OBJECT (msg), "currentSize",
			   GINT_TO_POINTER (cs));
	if (GPOINTER_TO_INT
	    (g_object_get_data (G_OBJECT (msg), "totalSize"))) {
		gtk_range_set_value (GTK_RANGE (gtk_builder_get_object
						(builder,
						 "downloadProgress")), cs);
	}
}

static void
DoCancelDownloadFlags ()
{
	soup_session_cancel_message (soupSession, soupMessage,
				     SOUP_STATUS_CANCELLED);
	EnableDisableDownloadButtons (FALSE);
}

static void
DoStartDownloadFlags ()
{
	if (soupSession == NULL) {
		soupSession =
		    soup_session_async_new_with_options
		    (SOUP_SESSION_SSL_CA_FILE, NULL,
		     SOUP_SESSION_PROXY_URI, NULL, NULL);
	}

	printf ("url: [%s]\n", FLAGS_URL);
	soupMessage = soup_message_new (SOUP_METHOD_GET, FLAGS_URL);

	g_signal_connect (G_OBJECT (soupMessage), "got_chunk",
			  (GCallback) GotMsgChunk, "chunk");
	g_signal_connect (G_OBJECT (soupMessage), "got_headers",
			  (GCallback) GotHeaders, NULL);

	soup_session_queue_message (soupSession, soupMessage,
				    (SoupSessionCallback) GotFile, NULL);

	EnableDisableDownloadButtons (TRUE);
	SetDownloadStatus (dgettext (PACKAGE, "Downloading ..."));
}

static void
CfgDlgResponse (GtkWidget * dialog, gint response_id)
{
	if (response_id) {
		if (response_id == GTK_RESPONSE_CLOSE)
			gtk_widget_destroy (dialog);
		StopListening ();

		builder = NULL;
		container = NULL;
	}
}

static void
ConfigurePlugin (GkbdIndicatorPluginContainer * pc, GtkWindow * parent)
{
	GtkWidget *dialog;
	GError *error = NULL;

	container = pc;

	LoadConfig ();

	builder = gtk_builder_new ();
	gtk_builder_add_from_file (builder, UI_DIR "/flags.ui", &error);

	dialog =
	    GTK_WIDGET (gtk_builder_get_object (builder, "flags_dialog"));
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	g_signal_connect (gtk_builder_get_object
			  (builder, "chkEnableFlags"), "toggled",
			  G_CALLBACK (ChangeShowFlag), NULL);
	g_signal_connect (gtk_builder_get_object
			  (builder, "btnStartDownload"), "clicked",
			  G_CALLBACK (DoStartDownloadFlags), NULL);
	g_signal_connect (gtk_builder_get_object
			  (builder, "btnCancelDownload"), "clicked",
			  G_CALLBACK (DoCancelDownloadFlags), NULL);
	g_signal_connect (G_OBJECT (dialog),
			  "response", G_CALLBACK (CfgDlgResponse), NULL);

	ConfigChangedCallback (NULL, 0, NULL, TRUE);
	StartGConfListening (TRUE);
	gtk_dialog_run (GTK_DIALOG (dialog));
}

G_MODULE_EXPORT const GkbdIndicatorPlugin *
GetPlugin (void)
{
	static GkbdIndicatorPlugin gswitchitPlugin = { NULL };
	if (gswitchitPlugin.name == NULL) {
		bindtextdomain (PACKAGE, GNOMELOCALEDIR);
		bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

		gswitchitPlugin.name = dgettext (PACKAGE, "Flags");
		gswitchitPlugin.description =
		    dgettext (PACKAGE,
			      "Downloads and enables flags in the applet");
		/* functions */
		gswitchitPlugin.init_callback = PluginInit;
		gswitchitPlugin.term_callback = PluginTerm;
		gswitchitPlugin.configure_properties_callback =
		    ConfigurePlugin;
	}
	return &gswitchitPlugin;
}
