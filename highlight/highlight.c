/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <gmodule.h>
#include <gnome.h>
#include <pango/pango.h>

#include "libgnomekbd/gkbd-indicator-plugin.h"

#define PLUGIN_GCONF_DIR "/apps/" PACKAGE "/Highlight"

#define FG_COLOR_CONFIG_KEY ( PLUGIN_GCONF_DIR "/ForegroundColor" )
#define BG_COLOR_CONFIG_KEY ( PLUGIN_GCONF_DIR "/BackgroundColor" )

static GkbdIndicatorPluginContainer *container;

static GdkColor bgColor = { 0L, 0, 0, 0x8000 };
static GdkColor fgColor = { 0L, 0xFFFF, 0xFFFF, 0xFFFF };

static guint listener;

static GtkBuilder *builder;

static void
LoadColor (GdkColor * color, const char *key)
{
	GError *gerror = NULL;
	gchar *str;
	str =
	    gconf_client_get_string (container->conf_client, key, &gerror);
	if (str == NULL || gerror != NULL) {
		if (gerror != NULL) {
			g_warning ("Error reading configuration:%s\n",
				   gerror->message);
			g_error_free (gerror);
			gerror = NULL;
		}
	} else {
		int r, g, b;

		if (sscanf (str, "%i %i %i", &r, &g, &b) == 3) {
			color->pixel = 0;
			color->red = r;
			color->green = g;
			color->blue = b;
		}
	}
}

static void
LoadConfig (void)
{
	LoadColor (&fgColor, FG_COLOR_CONFIG_KEY);
	LoadColor (&bgColor, BG_COLOR_CONFIG_KEY);
}

static void
PrepareSaveColor (GConfChangeSet * cs, GdkColor * color, const char *key)
{
	char buf[21];
	g_snprintf (buf, sizeof (buf), "%#04X %#04X %#04X",
		    color->red, color->green, color->blue);
	gconf_change_set_set_string (cs, key, buf);
}

static void
SaveConfig (void)
{
	GConfChangeSet *cs;
	GError *gerror = NULL;

	cs = gconf_change_set_new ();

	PrepareSaveColor (cs, &fgColor, FG_COLOR_CONFIG_KEY);
	PrepareSaveColor (cs, &bgColor, BG_COLOR_CONFIG_KEY);

	gconf_client_commit_change_set (container->conf_client, cs, TRUE,
					&gerror);
	if (gerror != NULL) {
		g_warning ("Error saving configuration: %s\n",
			   gerror->message);
		g_error_free (gerror);
		gerror = NULL;
	}
	gconf_change_set_unref (cs);
}

static void
ConfigChangedCallback (GConfClient * client,
		       guint cnxn_id, GConfEntry * entry)
{
	LoadConfig ();
	gkbd_indicator_plugin_container_reinit_ui (container);
}

static void
StartListening (void)
{
	GError *err = NULL;
	listener = gconf_client_notify_add (container->conf_client,
					    PLUGIN_GCONF_DIR,
					    (GConfClientNotifyFunc)
					    ConfigChangedCallback, NULL,
					    NULL, &err);
	if (0 == listener) {
		g_warning ("Error listening for configuration: [%s]\n",
			   err->message);
		g_error_free (err);
	}
}

static void
StopListening (void)
{
	gconf_client_notify_remove (container->conf_client, listener);
}

static gboolean
PluginInit (GkbdIndicatorPluginContainer * pc)
{
	GError *gerror = NULL;
	container = pc;

	gconf_client_add_dir (container->conf_client,
			      PLUGIN_GCONF_DIR,
			      GCONF_CLIENT_PRELOAD_NONE, &gerror);
	if (gerror != NULL) {
		g_error ("err: %s\n", gerror->message);
		g_error_free (gerror);
		gerror = NULL;
		return FALSE;
	}
	LoadConfig ();
	StartListening ();
	return TRUE;
}

static void
PluginTerm (void)
{
	StopListening ();
	container = NULL;
}

static void
UpdateColorChooserButtons ()
{
	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnFgColor")), GTK_STATE_NORMAL,
			      &fgColor);
	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnFgColor")), GTK_STATE_ACTIVE,
			      &fgColor);
	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnFgColor")),
			      GTK_STATE_PRELIGHT, &fgColor);
	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnFgColor")),
			      GTK_STATE_SELECTED, &fgColor);

	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnBgColor")), GTK_STATE_NORMAL,
			      &bgColor);
	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnBgColor")), GTK_STATE_ACTIVE,
			      &bgColor);
	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnBgColor")),
			      GTK_STATE_PRELIGHT, &bgColor);
	gtk_widget_modify_bg (GTK_WIDGET
			      (gtk_builder_get_object
			       (builder, "btnBgColor")),
			      GTK_STATE_SELECTED, &bgColor);
}

static void
ChooseColor (GtkButton * btn, GdkColor * color)
{
	GtkWidget *dlgSelection;
	gint dr;
	dlgSelection =
	    gtk_color_selection_dialog_new (dgettext
					    (PACKAGE, "Choose the color"));
	gtk_color_selection_set_current_color (GTK_COLOR_SELECTION
					       (GTK_COLOR_SELECTION_DIALOG
						(dlgSelection)->colorsel),
					       color);
	gtk_window_set_modal (GTK_WINDOW (dlgSelection), TRUE);
	dr = gtk_dialog_run (GTK_DIALOG (dlgSelection));
	if (GTK_RESPONSE_OK == dr) {
		gtk_color_selection_get_current_color (GTK_COLOR_SELECTION
						       (GTK_COLOR_SELECTION_DIALOG
							(dlgSelection)->
							colorsel), color);
		UpdateColorChooserButtons ();
		SaveConfig ();
	}
	gtk_widget_destroy (dlgSelection);
}

static void
CfgDlgResponse (GtkWidget * dlg, gint response_id)
{
	if (response_id) {
		if (response_id == GTK_RESPONSE_CLOSE)
			gtk_widget_destroy (dlg);
		container = NULL;
	}
}

static void
ConfigurePlugin (GkbdIndicatorPluginContainer * pc, GtkWindow * parent)
{
	GtkWidget *dialog;
	GError *error = NULL;

	container = pc;

	LoadConfig ();

	builder = gtk_builder_new ();
	gtk_builder_add_from_file (builder, UI_DIR "/highlight.ui",
				   &error);

	dialog =
	    GTK_WIDGET (gtk_builder_get_object
			(builder, "highlight_dialog"));
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_window_set_transient_for (GTK_WINDOW (dialog), parent);

	g_signal_connect (GTK_WIDGET
			  (gtk_builder_get_object (builder, "btnFgColor")),
			  "clicked", G_CALLBACK (ChooseColor), &fgColor);
	g_signal_connect (gtk_builder_get_object (builder, "btnBgColor"),
			  "clicked", G_CALLBACK (ChooseColor), &bgColor);
	g_signal_connect (G_OBJECT (dialog),
			  "response", G_CALLBACK (CfgDlgResponse), NULL);

	UpdateColorChooserButtons ();

	gtk_widget_show (dialog);
}

static GtkWidget *
FindChildLabel (GtkWidget * widget)
{
	GtkWidget *child = widget;
	/* go down till first non-bin widget */
	while (GTK_IS_BIN (child)) {
		child = GTK_BIN (child)->child;
	}

	return GTK_IS_LABEL (child) ? child : NULL;
}

static GtkWidget *
HighlightWidget (GtkWidget * widget, const gint group, const char
		 *groupDescription, GkbdKeyboardConfig * config)
{
	GtkWidget *label = FindChildLabel (widget);

	if (label != NULL) {
		const char *oldlbl =
		    gtk_label_get_label (GTK_LABEL (label));
		PangoColor pfcolor =
		    { fgColor.red, fgColor.green, fgColor.blue };
		PangoColor pbcolor =
		    { bgColor.red, bgColor.green, bgColor.blue };
		gchar *fcolor = pango_color_to_string (&pfcolor);
		gchar *bcolor = pango_color_to_string (&pbcolor);

		char *markup =
		    g_markup_printf_escaped
		    ("<span foreground=\"%s\" background=\"%s\">%s</span>",
		     fcolor, bcolor, oldlbl);

		gtk_label_set_markup (GTK_LABEL (label), markup);

		g_free (markup);
		g_free (fcolor);
		g_free (bcolor);
	}

	return widget;
}

G_MODULE_EXPORT const GkbdIndicatorPlugin *
GetPlugin (void)
{
	static GkbdIndicatorPlugin gswitchitPlugin = { NULL };
	if (gswitchitPlugin.name == NULL) {
		bindtextdomain (PACKAGE, GNOMELOCALEDIR);
		bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

		gswitchitPlugin.name = dgettext (PACKAGE, "Highlighter");
		gswitchitPlugin.description =
		    dgettext (PACKAGE,
			      "Highlights the applet on the GNOME panel");
		/* functions */
		gswitchitPlugin.init_callback = PluginInit;
		gswitchitPlugin.term_callback = PluginTerm;
		gswitchitPlugin.decorate_widget_callback = HighlightWidget;
		gswitchitPlugin.configure_properties_callback =
		    ConfigurePlugin;
	}
	return &gswitchitPlugin;
}
